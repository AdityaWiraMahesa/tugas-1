import 'dart:ui';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white70,
          title: Text(
            "Mai Melali",
            style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'Script'),
          ),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                onPressed: () {}),
            IconButton(
                icon: new Icon(Icons.settings, color: Colors.black),
                onPressed: () {}),
          ],
        ),

        //body
        body: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              //Countener Foto 1
              Container(
                // color: Colors.amber,
                margin: EdgeInsets.only(top: 20),
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                decoration: BoxDecoration(
                    border: Border.all(width: 5, color: Colors.amber)),
                child: Image.asset("assets/Bendungan.jpg",
                    width: 300, height: 300),
              ),
              Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                    "Bendungan, Sidemen",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )),

              Container(
                height: 20,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.star,
                      color: Colors.yellow[700],
                    ),
                    Icon(Icons.star, color: Colors.yellow[700]),
                    Icon(Icons.star, color: Colors.yellow[700]),
                    Icon(Icons.star, color: Colors.yellow[700]),
                    Icon(Icons.star_half_outlined, color: Colors.yellow[700]),
                  ],
                ),
              ),

              //Keterangan
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("More Information"),
                    //Button About
                    RaisedButton(
                        color: Colors.blue[800],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        padding: const EdgeInsets.fromLTRB(70, 10, 70, 10),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(80)),
                          padding: EdgeInsets.all(0),
                          width: 110,
                          height: 20,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "About",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {}),

                    //Button Location
                    RaisedButton(
                        color: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        padding: const EdgeInsets.fromLTRB(70, 10, 70, 10),
                        child: Container(
                          padding: EdgeInsets.all(0),
                          width: 110,
                          height: 20,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Location",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {}),

                    //Contact
                    RaisedButton(
                        color: Colors.green[900],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        padding: const EdgeInsets.fromLTRB(70, 10, 70, 10),
                        child: Container(
                          padding: EdgeInsets.all(0),
                          width: 110,
                          height: 20,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Contact",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {})
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
